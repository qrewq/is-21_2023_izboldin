import java.util.Scanner;

public class S1-T12 {
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)){
            System.out.println("Введите расстояние пути в км: ");
            double a = scan.nextDouble();
            System.out.println("Введите средний расход топлива на 100км: ");
            double b = scan.nextDouble();
            System.out.println("Введите стоимость одного литра топлива: ");
            double c = scan.nextDouble();
            double d = a / 100 * b;
            double e = d * c;   
            System.out.println("Расход топлива: " + d);
            System.out.println("Расход денег: " + e);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }    
}
