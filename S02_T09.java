import java.util.ArrayList;
import java.util.Scanner;
public class S02_T09 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите количество строк: ");
        int n = scan.nextInt();
        ArrayList a = new ArrayList<>();
        scan.nextLine();
        for (int i = 0; i < n; i++) {
            System.out.println("Строка" + " " + i + " " + ":");
            String b = scan.nextLine();
            a.add(b);
        }
        System.out.print(a);
    }
}