import java.util.Scanner;
public class S02_T04 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите A: ");
        int a = scan.nextInt();
        System.out.println("Введите B: ");
        int b = scan.nextInt();
        for (int i = 0; i < b; i++) {
            for (int j = 0; j < a; j++) {
                if (i == 0 || i == b - 1 || j == 0 || j == a - 1) {
                    System.out.print('*');
                } else {
                    System.out.print(' ');
                }
            }
            System.out.print("\n\r");
        }
    }
}