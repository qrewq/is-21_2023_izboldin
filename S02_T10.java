import java.util.Scanner;
public class S02_T10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите предложение: ");
        String text = scan.nextLine().toLowerCase();
        String a1 = "жы";
        String a2 = "шы";
        String b1 = "жи";
        String b2 = "ши";
        if (text.contains(b1) || text.contains(b2)) {
            System.out.println("Вы не ошиблись");
        } else if (text.contains(a1) || text.contains(a2)) {
            System.out.println("Ошибка! Жи/Ши пиши с буквой и");
        }
    }
}