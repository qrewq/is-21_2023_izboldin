import java.util.Scanner;

public class S01-T01 {
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)){
            System.out.println("Введите расстояние в см: ");
            int sm = scan.nextInt();
            System.out.println(sm / 100 + " метров");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
