import java.util.Scanner;
public class S02_T05 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите слово: ");
        String a = scan.nextLine();
        if (a.equalsIgnoreCase(new StringBuffer(a).reverse().toString())) {
            System.out.println("Палиндром");
        } else {
            System.out.println("Не палиндром");
        }
    }
}