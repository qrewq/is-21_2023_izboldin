import java.util.Scanner;
public class S02_T06 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите строку: ");
        String str = scan.nextLine();
        System.out.println("Введите символ: ");
        char symbol = scan.next().charAt(0);
        int n = 0;
        char N = Character.toUpperCase(symbol);
        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == symbol){
                n++;
            }
        }
        System.out.println("Кол-во вхождений:" + n);
        System.out.println("Преобразованная строка:" + str.replace(symbol, N));
    }
}