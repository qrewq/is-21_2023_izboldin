import java.util.Scanner;

public class S01-T02 {
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)) {
            System.out.println("Введите число прощедших секунд: ");
            int sec = scan.nextInt();
            System.out.println(sec / 3600);
            System.out.println(sec % 3600 / 60);
            System.out.println(sec % 3600 % 60);
        } catch(Exception e) {
        e.printStackTrace();
        }
    }
}
