import java.util.Scanner;

public class S01-T08 {
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)){
            System.out.println("Введите n: ");
            int n = scan.nextInt();
            for (int a = 1; a < 11; a++)
                System.out.println(n + " * " + a + " = " + n * a);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }    
}
