import java.util.Scanner;

public class S01-T04 {
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)){
            System.out.println("Введите x1 y1: ");
            double x1 = scan.nextDouble();
            double y1 = scan.nextDouble();
            System.out.println("Введите x2 y2: ");
            double x2 = scan.nextDouble();
            double y2 = scan.nextDouble();
            System.out.println("Расстояние между ними: " + Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2)));
        
        } catch(Exception e) {
            e.printStackTrace();
        }
    }    
}
