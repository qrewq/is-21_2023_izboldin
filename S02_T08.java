import java.util.Scanner;
public class S02_T08 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите строку: ");
        String str = scan.nextLine();
        StringBuilder str1 = new StringBuilder();
        for(int i = str.length() - 1; i >= 0; i--){
            str1.append(str.charAt(i));
        }
        System.out.println("Результат инверсии: " + str1);
    }
}