import java.util.Scanner;

public class S01-T10 {
    public static void main(String[] args) {    
        try(Scanner scan = new Scanner(System.in)){
            System.out.println("Введите длину массива: ");
            int size = scan.nextInt();
            int array[] = new int[size];
            System.out.println("Заполните массив элементами: ");
            int a = 0;
            int b = 1;
            for (int i = 0; i < size; i++) {
                array[i] = scan.nextInt();
            }
            for (int l = 0; l < size; l++) {
                if(array[l] % 2 == 0) {
                    a = a + array[l];
                }
                else {
                    b = b * array[l];
                }
            }
            System.out.println("Сумма четных чисел:" + a);
            System.out.println("Произведение" + b);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }    
}
