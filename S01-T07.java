import java.util.Scanner;

public class S01-T07 {
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)){
            System.out.println("Введите x: ");
            double x = scan.nextDouble();
            if(x > 0){
                double y = Math.pow(Math.sin(x), 2);
                System.out.println("y = " + y);
            }
            else{
                double y = 1 - 2 * Math.sin(x * x);
                System.out.println("y = " + y);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }    
}    
